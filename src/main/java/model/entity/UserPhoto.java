package model.entity;

import java.awt.*;
import java.awt.image.BufferedImage;

public class UserPhoto {
    private BufferedImage image;
    private String path;
    private Color color;

    public UserPhoto(BufferedImage image, String path, Color color) {
        this.image = image;
        this.path = path;
        this.color = color;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
