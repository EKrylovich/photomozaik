package model.logic.mysql;

import model.entity.UserPhoto;
import model.logic.dao.PhotoDAO;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;


public class MySqlPhotoDao implements PhotoDAO {
    private Connection connection;
    private String customTable;

    private String ADD_PHOTO_QUERY;
    private String DELETE_PHOTO_QUERY;
    private String DROP_TABLE;
    private String CREATE_TABLE;
    private String FIND_QUERY_WITH_DELETE;
    private String FIND_QUERY_WITHOUT_DELETE;
    private String COUNT_QUERY;
    private String UPDATE_COUNTER_QUERY;


    public MySqlPhotoDao(String customTable, Connection connection) {
        this.customTable = customTable;
        this.connection = connection;
        initSQLQuery();
        try {
            executeSimpleSql(CREATE_TABLE);
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("Database connection is failed " + e);
        }
    }

    private void initSQLQuery() {
        ADD_PHOTO_QUERY = "INSERT INTO `photomosaic`.`" + customTable + "` (R,G,B,path) values (?,?,?,?);";
        DELETE_PHOTO_QUERY = "DELETE FROM `photomosaic`.`" + customTable + "` WHERE `path` = ? ;";
        DROP_TABLE = "DROP TABLE `photomosaic`.`" + customTable + "`;";
        CREATE_TABLE = "CREATE TABLE `photomosaic`.`" + customTable + "` (`path` VARCHAR(300) NOT NULL," +
                "`R` INT NOT NULL,`G` INT NOT NULL,`B` INT NOT NULL,`COUNTER` INT NOT NULL DEFAULT 0," +
                " PRIMARY KEY (`path`));";
        COUNT_QUERY = "SELECT count(path)as count FROM `photomosaic`.`" + customTable + "`;";
        FIND_QUERY_WITH_DELETE = "SELECT  path,sqrt((pow((R - ?),2)+pow((G - ?),2)+pow((B - ?),2))) AS AAA FROM " +
                "`photomosaic`.`" + customTable + "` ORDER BY AAA LIMIT 1 ;";
        FIND_QUERY_WITHOUT_DELETE = "SELECT path FROM( SELECT  path,(COUNTER) AS u,sqrt((pow((R - ?),2)+" +
                "pow((G - ?),2)+pow((B - ?),2))) AS AAA FROM `photomosaic`.`" + customTable + "`  ORDER BY AAA LIMIT 5)" +
                " AS a ORDER BY u LIMIT 1";
        UPDATE_COUNTER_QUERY = "update `photomosaic`.`" + customTable + "` set COUNTER = COUNTER + 1 where path = ?; ";
    }


    @Override
    public void addPhoto(UserPhoto photo) {
        try (PreparedStatement statement = connection.prepareStatement(ADD_PHOTO_QUERY)) {
            setStatementRGB(statement, photo.getColor());
            statement.setString(4, photo.getPath());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("SQL isn' t correct " + e);
        }
    }

    private void setStatementRGB(PreparedStatement statement, Color color, int shift) throws SQLException {
        statement.setDouble(1 + shift, color.getRed());
        statement.setDouble(2 + shift, color.getGreen());
        statement.setDouble(3 + shift, color.getBlue());
    }

    private void setStatementRGB(PreparedStatement statement, Color color) throws SQLException {
        setStatementRGB(statement, color, 0);
    }

    private String findPathToNearestPhoto(Color color,String query) {
        String path = null;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            setStatementRGB(statement, color);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                path = set.getString("path");
            }
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("Oops, something wrong  " + e);
        }
        return path;
    }

    private void increment(String path) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_COUNTER_QUERY)) {
            statement.setString(1, path);
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("SQL isn' t correct " + e);
        }
    }


    @Override
    public BufferedImage findNearestPhotoWithDelete(Color color) {
        BufferedImage image = findImage(color,FIND_QUERY_WITH_DELETE);
        return image;
    }

    private BufferedImage findImage(Color color, String FIND_QUERY){
        String photoPath = null;
        BufferedImage image = null;
        boolean isPhotoExist = false;
        while (!isPhotoExist) {
            try {
                photoPath = findPathToNearestPhoto(color,FIND_QUERY);
                try (InputStream stream = new FileInputStream(new File(photoPath))) {
                    image = ImageIO.read(stream);
                }
                isPhotoExist = true;
            } catch (Exception e) {
                //TODO прикрутить логер
                System.out.println("File " + photoPath + " not found " + e);
                deletePhotoByPath(photoPath);
                findPathToNearestPhoto(color,FIND_QUERY);
            } finally {
                if (FIND_QUERY.equals(FIND_QUERY_WITH_DELETE)){
                    deletePhotoByPath(photoPath);
                }else {
                    increment(photoPath);
                }
            }
        }
        return image;

    }

    @Override
    public BufferedImage findNearestPhotoWithoutDelete(Color color) {
        BufferedImage image = findImage(color,FIND_QUERY_WITHOUT_DELETE);
        return image;
    }

    @Override
    public void destroyStorage() {
        try {
            executeSimpleSql(DROP_TABLE);
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("SQL exception " + e);
        }
    }

    public void deletePhotoByPath(String path) {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_PHOTO_QUERY)) {
            statement.setString(1, path);
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("Photo " + path + " wasn't deleted" + e);
        }
    }

    private void executeSimpleSql(String sql) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    @Override
    public int count() {
        try (Statement statement = connection.createStatement()) {
            statement.execute(COUNT_QUERY);
            ResultSet set = statement.getResultSet();
            if (set.next()) {
                return set.getInt("count");
            }
        } catch (SQLException e) {
            //TODO прикрутить логер
            System.out.println("Oops, something wrong  " + e);
        }
        return 0;
    }
}
