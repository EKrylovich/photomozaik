package model.logic.dao;

public interface DaoFactory {
    PhotoDAO getDao(String customTable);
}
