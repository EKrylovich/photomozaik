package model.logic.dao;

import model.entity.UserPhoto;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public interface PhotoDAO {

    void addPhoto(UserPhoto photo);

    BufferedImage findNearestPhotoWithDelete(Color color);

    BufferedImage findNearestPhotoWithoutDelete(Color color);

    void destroyStorage();

    int count();
}
