package model.logic.mongodb;

import model.logic.dao.DaoFactory;
import model.logic.dao.PhotoDAO;
import model.logic.mysql.MySqlDaoFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;


public class MongoDBDaoFactory implements DaoFactory {

    public static final String DATABASE_PROPERTIES_FILE_PATH = "/MongoDB.properties";

    private String host;
    private String port;
    private String DB;
    private String login;
    private String password;

    private static MongoDBDaoFactory instance;

    public static MongoDBDaoFactory getInstanse() {
        if (instance == null) {
            instance = new MongoDBDaoFactory();
        }
        return instance;
    }

    private MongoDBDaoFactory() {
        readProperties();
    }

    private void readProperties() {
        try (InputStream stream = MySqlDaoFactory.class.getResourceAsStream(DATABASE_PROPERTIES_FILE_PATH)) {
            Properties prop = new Properties();
            prop.load(stream);
            host = prop.getProperty("host");
            port = prop.getProperty("port");
            DB =  prop.getProperty("dbname");

        } catch (IOException e) {
        }
    }
    @Override
    public PhotoDAO getDao(String customTable) {
        try {

            return new MongoDBPhotoDao(customTable,host,port,DB);
        }
        catch (UnknownHostException e){

        }

        return null;
    }
}
