package model.logic.mongodb;

import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;
import model.entity.UserPhoto;
import model.logic.dao.PhotoDAO;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;


public class MongoDBPhotoDao implements PhotoDAO {
    private static int count = 0;
    private DB db;
    private DBCollection collection;

    public MongoDBPhotoDao(String customTable,String host,String port,String DB) throws UnknownHostException{
        MongoClient mongo  = new MongoClient(host,Integer.valueOf(port));
        db = mongo.getDB(DB);
        collection = db.getCollection(customTable);
        collection.createIndex(new BasicDBObject("location", "2dsphere"));
    }


    @Override
    public  void addPhoto(UserPhoto photo) {

        int r = photo.getColor().getRed();
        int g = photo.getColor().getGreen();
        int b = photo.getColor().getBlue();
        BasicDBObject doc = new BasicDBObject("name","photo"+count)
                .append("location", new BasicDBObject("type", "Point")
                        .append("coordinates",new double[]{Math.sqrt(r*r+b*b+g*g)/10,0}));
        collection.insert(doc);
        GridFS gfsPhoto = new GridFS(db, "photo");
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(photo.getImage(), "gif", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            GridFSInputFile gfsFile = gfsPhoto.createFile(is);
            gfsFile.setFilename("photo"+count);
            gfsFile.save();
            count++;
        }
        catch(IOException e){

        }

    }

    @Override
    public void destroyStorage() {
        db.getCollection("photo.files").drop();
        db.getCollection("photo.chunks").drop();
        collection.drop();
    }

    @Override
    public BufferedImage findNearestPhotoWithDelete(Color color) {
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();
        //BasicDBObject filter = new BasicDBObject("$near", new double[] { Math.sqrt(r*r+g*g+b*b)/10, 0 });
        //BasicDBObject query = new BasicDBObject("loc", filter);
        DBObject query = BasicDBObjectBuilder.start()
                .push("location")
                .push("$near")
                .push("$geometry")
                .add("type", "Point")
                .add("coordinates",  new double[] { Math.sqrt(r*r+g*g+b*b)/10, 0 })
                .get();
        DBCursor cursor = collection.find(query);
        if(cursor.hasNext()) {
            DBObject o = cursor.next();
            String name  = o.get("name").toString();


            GridFS gfsPhoto = new GridFS(db, "photo");
            try {
                BufferedImage image = ImageIO.read(gfsPhoto.findOne(name).getInputStream());
                gfsPhoto.remove(gfsPhoto.findOne(name));
                collection.remove(o);
                return image;
            }
            catch(IOException e) {
                return null;
            }
        }
        return null;
    }

    @Override
    public BufferedImage findNearestPhotoWithoutDelete(Color color) {
        return null;
    }

    @Override
    public int count() {
        return (int)collection.count();

    }


}

