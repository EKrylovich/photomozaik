package model.logic.service;


import controllers.requestHandlers.userSelections.UserData;
import model.logic.mosaic.MosaicBuilderSingleThread;
import model.multithreading.PhotoProcessing;
import model.multithreading.UserPhotoStore;
import model.multithreading.ServiceAdder;
import model.multithreading.FileStore;
import model.logic.mosaic.MosaicBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainClass {
    private int THREAD_COUNT = Runtime.getRuntime().availableProcessors() * 2;
    private String pathToImage;
    private String uniqueFolder;
    private UserPhotoStore photoStore;
    private FileStore fileStore;
    private int photoHeight;

    public MainClass(String pathToImage, List<File> photos, String uniqueFolder, int photoHeight) {
        this.pathToImage = pathToImage;
        uniqueFolder = uniqueFolder.replaceAll("\\W", "");
        this.uniqueFolder = uniqueFolder.substring(uniqueFolder.length() / 2);
        fileStore = new FileStore(photos);
        photoStore = new UserPhotoStore();
        this.photoHeight = photoHeight;
    }

    public String getPathToReadyPuzzle(UserData data) {
        PhotoService service = new MySQLPhotoService(uniqueFolder);
        Date d1 = new Date();
        putImageInTheService(service,data);
        // TODO BuilderTeam тут полученный файл превращаем в картинку и засовываем в ваш новый класс для расчетов

        //...some code...

        //TODO полученый DTO передае в конструктор мозайки и из него вытаскиваем что нам нужно
        Date d2 = new Date();
        System.out.println("Добавление в сервис");
        System.out.println((d2.getTime()-d1.getTime())/1000);
        MosaicBuilder builder = new MosaicBuilderSingleThread(service, pathToImage, photoHeight, data.getUniqueFlag());
        String pathToPuzzle = builder.createMosaic();
        Date d3 = new Date();
        System.out.println("Создание мозайки");
        System.out.println((d3.getTime()-d2.getTime())/1000);
        service.destroyStorage();
        return pathToPuzzle;

    }

    private void putImageInTheService(PhotoService service, UserData data) {
        List<Thread> processingThread = new ArrayList<>();
        for (int i = 0; i <THREAD_COUNT ; i++) {
            processingThread.add(new Thread(new PhotoProcessing(fileStore, photoStore, data)));
        }

        ServiceAdder support = new ServiceAdder(photoStore, service);

        for (Thread currentThread : processingThread) {
            currentThread.start();
        }

        Thread serviceSupport = new Thread(support);
        serviceSupport.start();

        for (Thread currentThread : processingThread) {
            try {
                currentThread.join();
            } catch (InterruptedException e) {
                System.out.println("Thread was interrupted " + e);
            }
        }
        support.finish();
        try {
            serviceSupport.join();
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted " + e);
        }
    }
//
//    public static void main(String[] args) {
//
//        String path = "G:\\images";
//        File folder = new File(path);
//        String[] list = folder.list();
//        List<File> files = new ArrayList<>();
//
//        for (String fileName : list) {
//            files.add(new File(path + "\\" + fileName));
//        }
//
//        FileStore store = new FileStore(files);
//        UserPhotoStore preparedStore = new UserPhotoStore();
//        PhotoService service = new MySQLPhotoService("test");
//
//        Thread thread1 = new Thread(iA1);
//        Thread thread2 = new Thread(iA2);
//        Thread thread3 = new Thread(iA3);
//        Thread thread4 = new Thread(iA4);
//        Thread supportThread = new Thread(support);
//
//        thread1.start();
//        thread2.start();
//        thread3.start();
//        thread4.start();
//        supportThread.start();
//        supportThread.setName("support");
//
//        try {
//            thread1.join();
//            thread2.join();
//            thread3.join();
//            thread4.join();
//            support.finish();
//            supportThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//

}
