package model.logic.service;

import model.entity.UserPhoto;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public interface PhotoService {

    void addPhoto(UserPhoto photo);

    BufferedImage findPathNearestPhotoWithDelete(Color color);

    BufferedImage findPathNearestPhotoWithoutDelete(Color color);

    void destroyStorage();

    int count();

}
