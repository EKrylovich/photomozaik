package model.logic.service;

import model.entity.UserPhoto;
import model.logic.dao.PhotoDAO;
import model.logic.mysql.MySqlDaoFactory;

import java.awt.*;
import java.awt.image.BufferedImage;


public class MySQLPhotoService implements PhotoService {
    private MySqlDaoFactory factory;
    private PhotoDAO dao;

    public MySQLPhotoService(String customerTable) {
        factory = MySqlDaoFactory.getInstanse();
        this.dao = factory.getDao(customerTable);
    }

    @Override
    public void addPhoto(UserPhoto photo) {
        dao.addPhoto(photo);
    }


    @Override
    public BufferedImage findPathNearestPhotoWithDelete(Color color) {
        return  dao.findNearestPhotoWithDelete(color);
    }


    @Override
    public BufferedImage findPathNearestPhotoWithoutDelete(Color color) {
        return  dao.findNearestPhotoWithoutDelete(color);
    }

    @Override
    public void destroyStorage() {
        dao.destroyStorage();
    }

    @Override
    public int count() {
        return dao.count();
    }
}
