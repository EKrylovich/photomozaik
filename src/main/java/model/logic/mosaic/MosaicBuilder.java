package model.logic.mosaic;

public interface MosaicBuilder {
    String createMosaic();
}
