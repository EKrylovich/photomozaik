package model.logic.mosaic;

import model.logic.service.PhotoService;

import java.awt.*;
import java.awt.image.BufferedImage;

//TODO протестировать, если повезет, то все здорово
public class MosaicBuilderMultiThreads implements MosaicBuilder, Runnable {

    private PhotoService service;
    private Color[] colors;
    private int puzzleWidth;
    private int puzzleHeight;
    private int index;
    private BufferedImage[] mosaicParts;

    public MosaicBuilderMultiThreads(PhotoService service, Color[] colors, int puzzleWidth, int puzzleHeight, BufferedImage[] mosaicParts, int index) {
        this.service = service;
        this.colors = colors;
        this.puzzleWidth = puzzleWidth;
        this.puzzleHeight = puzzleHeight;
        this.mosaicParts = mosaicParts;
        this.index = index;
    }

    private void saveMosaicPart(BufferedImage part) {
        synchronized (mosaicParts) {
            mosaicParts[index] = part;
        }
    }

    @Override
    public void run() {
        BufferedImage mosaicPart = new BufferedImage(puzzleWidth, puzzleHeight * colors.length, BufferedImage.TYPE_INT_ARGB);
        Graphics g = mosaicPart.getGraphics();
        for (int i = 0; i < colors.length; i++) {
            g.drawImage(getPreparedImage(colors[i]), 0, puzzleHeight * i, puzzleWidth, puzzleHeight, null);
        }
        g.dispose();
        saveMosaicPart(mosaicPart);
    }
    private BufferedImage getPreparedImage(Color color) {
        BufferedImage image = service.findPathNearestPhotoWithoutDelete(color);
        BufferedImage puzzle;
/*

        int height = image.getHeight();
        int width = image.getWidth();

        double relation = width * 1.0 / height;
        double puzzleRelation = imageData.getPuzzleWidth() * 1.0 / imageData.getPuzzleHeight();

        if (Math.abs(relation - puzzleRelation) < 0.01) {
            return image;
        } else if (relation < puzzleRelation) {
            //todo ресайз по ширине
            int tmpHeight = (int) (width / puzzleRelation);
            puzzle = image.getSubimage(0, (height - tmpHeight) / 2, width, tmpHeight);
        } else {
            //todo ресайз по высоте
            int tmpWidth = (int) (puzzleRelation * height);
            puzzle = image.getSubimage((width - tmpWidth) / 2, 0, tmpWidth, height);
        }
*/

        return image;// puzzle;
    }


    @Override
    public String createMosaic() {
        return null;
    }
}