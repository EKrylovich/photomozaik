package model.logic.mosaic;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageDataObject {

    private final double RESERVE_KOEFF = 0.7;
    private int MIN_VALUE = 400;
    //исходная картинка
    private String photoPath;
    private BufferedImage masterPhoto;
    private int photoHeight;
    private int photoWidth;
    private double relation;

    private int newPhotoHeight;
    private int newPhotoWidth;

    private int countWidth;
    private int countHeight;
    private int puzzleWidth;
    private int puzzleHeight;

    public ImageDataObject(String photoPath) {
        this.photoPath = photoPath;
        this.masterPhoto = getImageByPath(photoPath);
        photoWidth = masterPhoto.getWidth();
        photoHeight = masterPhoto.getHeight();
        relation = ((double) photoWidth) / photoHeight;
    }

    //TODO скорее всего этим методом не будем пользоваться, но пока что его оставим
    private BufferedImage getImageByPath(String path) {
        BufferedImage image = null;
        try (InputStream stream = new FileInputStream(path)) {
            image = ImageIO.read(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public void calculatePuzzleParameters(int count) {

        System.out.println("Puzzle count " + count);
        if (count > MIN_VALUE) {
            count = (int) (count * RESERVE_KOEFF);
        }

        countWidth = (int) Math.sqrt(count);
        countHeight = countWidth;//(int) Math.sqrt(count);

        puzzleWidth = photoWidth / countWidth;
        puzzleHeight = photoHeight / countHeight;

        System.out.println(countWidth + " ширина n");
        System.out.println(countHeight + "высота n");
        System.out.println(puzzleWidth + "ширина картинки");
        System.out.println(puzzleHeight + " высота картинки");
    }

    public void calculateNewPuzzleParameters() {
        if (newPhotoWidth != photoWidth && newPhotoHeight != photoHeight) {
            puzzleWidth = puzzleWidth * newPhotoWidth / photoWidth;
            puzzleHeight = puzzleHeight * newPhotoHeight / photoHeight;
        }
        System.out.println(puzzleWidth + " новая ширина картинки");
        System.out.println(puzzleHeight + " новая высота картинки");
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public int getPhotoWidth() {
        return photoWidth;
    }

    public int getPhotoHeight() {
        return photoHeight;
    }

    public int getNewPhotoHeight() {
        return newPhotoHeight;
    }

    public int getNewPhotoWidth() {
        return newPhotoWidth;
    }

    public int getCountWidth() {
        return countWidth;
    }

    public int getCountHeight() {
        return countHeight;
    }

    public int getPuzzleWidth() {
        return puzzleWidth;
    }

    public int getPuzzleHeight() {
        return puzzleHeight;
    }

    public BufferedImage getMasterPhoto() {
        return masterPhoto;
    }

    public BufferedImage getNewEmptyImage() {
        return new BufferedImage(puzzleWidth * countWidth, puzzleHeight * countHeight, BufferedImage.TYPE_INT_ARGB);

    }

    public void setNewPhotoHeight(int newPhotoHeight) {
        if (newPhotoHeight <= 0) {
            this.newPhotoHeight = this.photoHeight;
            this.newPhotoWidth = this.photoWidth;
        } else {
            this.newPhotoHeight = newPhotoHeight;
            this.newPhotoWidth = (int) (newPhotoHeight * relation);
        }
    }
}
