package model.logic.mosaic;

import model.logic.service.PhotoService;
import util.ColorCalculator;
import util.DirectoryHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MosaicBuilderSingleThread implements MosaicBuilder {
    private ImageDataObject imageData;
    private PhotoService service;
    private ColorCalculator calculator;
    private boolean uniqueFlag;


    public MosaicBuilderSingleThread(PhotoService service, String photoPath, int photoHeight, boolean uniqueFlag) {
        this.service = service;
        this.calculator = new ColorCalculator();
        this.imageData = new ImageDataObject(photoPath);
        imageData.setNewPhotoHeight(photoHeight);
        this.uniqueFlag = uniqueFlag;
    }

    public String createMosaic(Color[][] colors) {
        imageData.calculateNewPuzzleParameters();
        BufferedImage puzzle;

        BufferedImage mosaic = imageData.getNewEmptyImage();
        Graphics graphics = mosaic.getGraphics();

        for (int w = 0; w < colors.length; w++) {
            for (int h = 0; h < colors[w].length; h++) {
                do {
                    puzzle = getPreparedImage(colors[w][h]);//service.findPathNearestPhoto(colors[w][h]);
                } while (puzzle == null);
                graphics.drawImage(puzzle, w * imageData.getPuzzleWidth(), h * imageData.getPuzzleHeight(), imageData.getPuzzleWidth(), imageData.getPuzzleHeight(), null);
            }
        }
        graphics.dispose();


        File file = makeResizeAndSave(mosaic);
        return file.getAbsolutePath();
    }


    private File makeResizeAndSave(BufferedImage mosaic) {
        String photoPath = imageData.getPhotoPath();
        photoPath = photoPath.substring(0, photoPath.lastIndexOf(DirectoryHandler.FILE_SEPARATOR)) +
                DirectoryHandler.FILE_SEPARATOR + "done.png";

        File savedMosaic = new File(photoPath);

        BufferedImage resizeImage = new BufferedImage(imageData.getNewPhotoWidth(), imageData.getNewPhotoHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = resizeImage.createGraphics();

        g.drawImage(mosaic, 0, 0, imageData.getNewPhotoWidth(), imageData.getNewPhotoHeight(), null);
        g.dispose();
        try {
            ImageIO.write(resizeImage, "PNG", savedMosaic);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedMosaic;
    }

    //TODO из метода вынести всю логику для расчета параметров.
    @Override
    public String createMosaic() {

        imageData.calculatePuzzleParameters(service.count());
        BufferedImage masterPhoto = imageData.getMasterPhoto();

        //TODO оставить только эту часть
        Color colors[][] = new Color[imageData.getCountWidth()][imageData.getCountHeight()];

        int x, y, width, height;

        for (int h = 0; h < imageData.getCountHeight(); h++) {
            for (int w = 0; w < imageData.getCountWidth(); w++) {

                x = w * imageData.getPuzzleWidth();
                y = h * imageData.getPuzzleHeight();
                width = imageData.getPuzzleWidth();
                height = imageData.getPuzzleHeight();

                if ((x + width) > imageData.getPhotoWidth()) {
                    width = imageData.getPhotoWidth() - x;
                }
                if ((y + height) > imageData.getPhotoHeight()) {
                    height = imageData.getPhotoHeight() - y;
                }

                try {
                    colors[w][h] = calculator.averageColor(masterPhoto.getSubimage(x, y, width, height));
                } catch (Exception e) {
                    System.out.println("oops " + e);
                    //TODO придумать решение
                    colors[w][h] = new Color(0, 0, 0);
                }
            }
        }
        return createMosaic(colors);
    }

    private BufferedImage getPreparedImage(Color color) {
        BufferedImage image;
        BufferedImage puzzle;

        if (uniqueFlag) {
            image = service.findPathNearestPhotoWithDelete(color);
        } else {
            image = service.findPathNearestPhotoWithoutDelete(color);
        }

        int height = image.getHeight();
        int width = image.getWidth();

        double relation = width * 1.0 / height;
        double puzzleRelation = imageData.getPuzzleWidth() * 1.0 / imageData.getPuzzleHeight();

        if (Math.abs(relation - puzzleRelation) < 0.01) {
            return image;
        } else if (relation < puzzleRelation) {
            //todo ресайз по ширине
            int tmpHeight = (int) (width / puzzleRelation);
            puzzle = image.getSubimage(0, (height - tmpHeight) / 2, width, tmpHeight);
        } else {
            //todo ресайз по высоте
            int tmpWidth = (int) (puzzleRelation * height);
            puzzle = image.getSubimage((width - tmpWidth) / 2, 0, tmpWidth, height);
        }

        return puzzle;
    }

}
