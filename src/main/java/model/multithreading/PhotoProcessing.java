package model.multithreading;

import controllers.requestHandlers.userSelections.CalculatorType;
import controllers.requestHandlers.userSelections.UserData;
import model.entity.UserPhoto;
import util.Calculator;
import util.ColorCalculator;
import util.HistogramicColorCalculator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PhotoProcessing implements Runnable {
    private FileStore store;
    private UserPhotoStore preparedStore;
    private Calculator calculator;
    private UserData data;

    public PhotoProcessing(FileStore store, UserPhotoStore preparedStore, UserData data) {
        this.store = store;
        this.preparedStore = preparedStore;
        calculator = new ColorCalculator();
        if (data.getType().equals(CalculatorType.HISTOGRAM)){
            calculator = new HistogramicColorCalculator();
        }else {
           calculator = new ColorCalculator();
        }
        this.data = data;
    }

    @Override
    public void run() {
        while (store.size() != 0) {
            File file = store.getFile();
            BufferedImage img = castToImage(file);
            if (img == null) {
                continue;
            }
            Color color = calculator.averageColor(img,data.getAccuracy());

            preparedStore.putPhoto(new UserPhoto(img, file.getAbsolutePath(), color));
        }
    }

    private BufferedImage castToImage(File file) {
        BufferedImage image;
        try (InputStream stream = new FileInputStream(file)) {
            image = ImageIO.read(stream);
            return image;
        } catch (IOException e) {
            System.out.println(file + " is not a image");
        }
        return null;
    }
}
