package model.multithreading;

import model.entity.UserPhoto;
import model.logic.service.PhotoService;

public class ServiceAdder implements Runnable {
    private UserPhotoStore store;
    private PhotoService service;
    private boolean isUnaccountedFiles;

    public ServiceAdder(UserPhotoStore store, PhotoService service) {
        this.store = store;
        this.service = service;
        isUnaccountedFiles = true;
    }

    @Override
    public void run() {
        while (isUnaccountedFiles) {
            temp();
        }
        while (!store.isEmpty()){
            temp();
        }
    }

    private void temp(){
        UserPhoto photo = store.getPhoto();
        service.addPhoto(photo);
    }


    public synchronized void finish() {
        isUnaccountedFiles = false;
    }


}
