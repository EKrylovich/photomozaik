package model.multithreading;

import model.entity.UserPhoto;

import java.util.ArrayList;
import java.util.List;

public class UserPhotoStore {
    public UserPhotoStore() {
        this.list = new ArrayList<>();
    }

    private List<UserPhoto> list;

    public synchronized UserPhoto getPhoto() {
        UserPhoto currentPhoto = null;
        while (currentPhoto == null) {
            if (list.isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            } else {
                currentPhoto = list.get(0);
                list.remove(0);
            }
        }
        return currentPhoto;
    }

    public synchronized void putPhoto(UserPhoto photo) {
        list.add(photo);
        notifyAll();
    }

    public synchronized boolean isEmpty(){
        return list.isEmpty();
    }
}
