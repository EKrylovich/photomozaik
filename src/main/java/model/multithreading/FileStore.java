package model.multithreading;

import java.io.File;
import java.util.List;

public class FileStore {
    private List<File> files;

    public FileStore(List<File> files) {
        this.files = files;
    }

    public synchronized File getFile(){
        File currentFile = files.get(0);
        files.remove(0);
        return currentFile;
    }

    public synchronized int size(){
        return files.size();
    }
}
