package util;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

/**
 * Different directory and files methods
 * 
 * @author Chepelev Maksim
 *
 */
public final class DirectoryHandler {
	
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");

	private DirectoryHandler() {
		
	}
	
	/**
	 * Unzip only zip archives
	 * 
	 * @param zipSource zip file path
	 * @param unzipDestination destination for unzip files
	 * @throws ZipException
	 */
	public static void unzipFile(String zipSource, String unzipDestination) throws ZipException {

		ZipFile zipFile = new ZipFile(zipSource);
		zipFile.extractAll(unzipDestination);

	}

	//TODO переписать в стиле java8. тут возможно будет слабая производительность.
	//     либо переделать возвращаемые данные в формат file
	/**
	 * Get all files' path in directory and it's sub directories 
	 * 
	 * @param rootDir you know)
	 * @return list of files' path
	 */
	public static LinkedList<String> getFilesSpis(String rootDir) {

		File root = new File(rootDir);
		LinkedList<String> fileNames = new LinkedList<>();

		if (root.isDirectory()) {
			File[] files = root.listFiles();

			for (File f : files) {
				getOnlyFiles(f, fileNames);
			}
		} else {
			throw new RuntimeException();
		}
		return fileNames;




	}

	private static void getOnlyFiles(File currentFile, List<String> fNames) {

		if (currentFile.isDirectory()) {
			File[] files = currentFile.listFiles();

			for (File f : files) {
				getOnlyFiles(f, fNames);
			}
		} else {
			fNames.add(currentFile.getAbsolutePath());
		}

	}

	/**
	 * compares the extension with the correct images extensions
	 * 
	 * @param filename
	 * @return true if image extension is png, jpg or jpeg, else false
	 */
	public static boolean isImageFile(String filename) {

		String extension = FilenameUtils.getExtension(filename);

		//TODO возможно стоит вынести за метод или в конфиг файл
		ArrayList<String> correctExtentions = new ArrayList<>();
		correctExtentions.add("png");
		correctExtentions.add("jpg");
		//TODO Такое разрешение вообще бывает
		correctExtentions.add("jpeg");

		if (correctExtentions.contains(extension))
			return true;
		else
			return false;
	}

	/**
	 * select only images from group of files
	 * 
	 * @param files different files to check
	 * @return list of images
	 */	
	public static List<String> selectOnlyImageFiles(List<String> files) {

		//TODO переделывать! нельзя указывать тип здесь: он может отличаться от исходного!
		List<String> res = new LinkedList<>();

		for (String f : files) {
			if (isImageFile(f))
				res.add(f);

		}

		return res;

	}

	/**
	 * compares the extension with the correct archive extensions
	 * 
	 * @param filename
	 * @return true if image extension is zip, else false
	 */
	public static boolean isArchive(String filename) {

		String extension = FilenameUtils.getExtension(filename);

		//TODO возможно стоит вынести за метод
		ArrayList<String> correctExtentions = new ArrayList<>();
		correctExtentions.add("zip");

		if (correctExtentions.contains(extension))
			return true;
		else
			return false;
	}

	//TODO это не util
	/**
	 * Creates unique directory in directory, configured in web.xml ("images_directory" parameter),
	 * for current request
	 * 
	 * @param request
	 * @param uniquePart
	 * @return created directory's path
	 */
	public static String createRequestUniqueDir(HttpServletRequest request, String uniquePart) {

		String rootDirectory = request.getServletContext().getRealPath("/")
				 			 + request.getServletContext().getInitParameter("images_directory");
		String uniqueDirectory = uniquePart + File.separator;
	/*	String uniqueDirectory = Thread.currentThread().getName().toString() + " "
				 			   + (new Date()).toString() + FILE_SEPARATOR;
		uniqueDirectory = uniqueDirectory.replace(':', '-');
		uniqueDirectory = uniqueDirectory.replace(' ', '_');*/
		
		//TODO поменять местами переменные, иначе тяжело читать
		rootDirectory += uniqueDirectory;

		File f = new File(rootDirectory);
		f.mkdirs();

		//TODO временно для тестов
		System.out.println(rootDirectory);
		
		return rootDirectory;
		
	}

	
	//use only for test
	public static void main(String[] args) {
		List<String> res = new LinkedList<>();
		res = getFilesSpis("C:\\Users\\USER\\Desktop\\Altium Designer - Видеоуроки");
		System.out.println(res);
	}

}
