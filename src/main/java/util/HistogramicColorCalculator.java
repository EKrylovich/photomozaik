package util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

class Point {
    static int maxct = 1;
    int coords[] = new int[3];
    int n = 3;
    int ct = 1;

    public void setCoords(int[] coords) {
        this.coords[0] = coords[0];
        this.coords[1] = coords[1];
        this.coords[2] = coords[2];
    }

    @Override
    public boolean equals(Object obj) {
        Point o = (Point) obj;
        if (coords[0] != o.coords[0])
            return false;
        if (coords[1] != o.coords[1])
            return false;
        if (coords[2] != o.coords[2])
            return false;
        o.ct++;
        if (o.ct > maxct) {
            maxct = o.ct;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coords);
    }
}

class Cluster {
    ArrayList<Point> points = new ArrayList<Point>();
    Point center = new Point();
    int n;

    public void setPoints(ArrayList<Point> points) {
        this.points = new ArrayList<Point>(points);
    }
}

public class HistogramicColorCalculator implements Calculator {
    private ArrayList<Point> getPoints(Raster r) {
        HashSet<Point> points = new HashSet<Point>();
        int array[] = new int[4];
        for (int x = 0; x < r.getWidth(); x++) {
            for (int y = 0; y < r.getHeight(); y++) {
                Point p = new Point();
                p.setCoords(r.getPixel(x, y, array));
                points.add(p);
            }
        }
        return new ArrayList<>(points);
    }

    private double euclidean(Point p1, Point p2) {
        return Math.sqrt((p1.coords[0] - p2.coords[0]) * (p1.coords[0] - p2.coords[0]) +
                (p1.coords[1] - p2.coords[1]) * (p1.coords[1] - p2.coords[1]) +
                (p1.coords[2] - p2.coords[2]) * (p1.coords[2] - p2.coords[2]));
    }

    private Point calculate_center(ArrayList<Point> points, int n) {
        int[] vals = new int[n];
        int plen = 0;
        for (Point p : points) {
            plen += p.ct;
            for (int i = 0; i < n; i++)
                vals[i] += (p.coords[i] * p.ct);
        }
        Point p = new Point();
        p.coords[0] = vals[0] / plen;
        p.coords[1] = vals[1] / plen;
        p.coords[2] = vals[2] / plen;
        p.n = n;
        p.ct = 1;
        return p;
    }


    private ArrayList<Point> getRandomElements(final int amount, final ArrayList<Point> list) {
        ArrayList<Point> returnList = new ArrayList<Point>();
        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < amount; i++) {
            returnList.add(list.get(r.nextInt(list.size())));
        }
        return returnList;
    }

    private ArrayList<Cluster> kmeans(ArrayList<Point> points, int k, int min_diff) {
        if (k > 20) {
            k = 20;
        }
        if (k == 0) {
            k = 1;
        }
        ArrayList<Cluster> clusters = new ArrayList<Cluster>();
        for (Point p : getRandomElements(k, points)) {
            Cluster c = new Cluster();
            c.center = p;
            c.n = p.n;
            c.points.add(p);
            clusters.add(c);
        }
        while (true) {
            ArrayList<ArrayList<Point>> plists = new ArrayList<ArrayList<Point>>();
            for (int i = 0; i < k; i++) {
                plists.add(new ArrayList<Point>());
            }
            int m = 0;
            for (Point p : points) {
                double smallest_distance = Double.POSITIVE_INFINITY;
                for (int i = 0; i < k; i++) {
                    double distance = euclidean(p, clusters.get(i).center);
                    if (distance < smallest_distance) {
                        smallest_distance = distance;
                        m = i;
                    }
                }
                plists.get(m).add(p);
            }
            double diff = 0;
            for (int i = 0; i < k; i++) {
                Cluster old = clusters.get(i);
                Point center = calculate_center(plists.get(i), old.n);
                Cluster newest = new Cluster();
                newest.setPoints(plists.get(i));
                newest.center = center;
                newest.n = old.n;
                clusters.set(i, newest);
                diff = Math.max(diff, euclidean(old.center, newest.center));
            }
            if (diff < min_diff)
                break;
        }
        return clusters;

    }

    @Override
    public Color averageColor(BufferedImage image, int accuracy) {
        if (image == null) {
            System.out.println("Fuuuuck");
        }
        ArrayList<Point> points;
        final Raster raster = image.getRaster();
        points = getPoints(raster);
        ArrayList<Cluster> km = kmeans(points, raster.getHeight() * raster.getWidth() / Point.maxct, 1);
        int k = 0, max = 0;
        for (int i = 0; i < km.size(); i++) {
            if (km.get(i).points.size() > max) {
                max = km.get(i).points.size();
                k = i;
            }
        }
        return new Color(km.get(k).center.coords[0], km.get(k).center.coords[1], km.get(k).center.coords[2]);
    }
}