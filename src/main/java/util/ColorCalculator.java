package util;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class ColorCalculator implements Calculator {
    private final int CAST_TO_PERCENT = 100;

    public Color averageColor(BufferedImage image) {
        Color color = averageColor(image, 100);


        return color;
    }

    private int calculateWorkScope(int size, int accuracy) {
        return (size - (size * accuracy / CAST_TO_PERCENT));
    }

    @Override
    public Color averageColor(BufferedImage image, int accuracy) {
        int startPixelWidth;
        int stopPixelWidth;
        int startPixelHeight;
        int stopPixelHeight;
        if (accuracy == 100) {
            startPixelWidth = 0;
            stopPixelWidth = image.getWidth();
            startPixelHeight = 0;
            stopPixelHeight = image.getHeight();
        } else {
            startPixelWidth = calculateWorkScope(image.getWidth(), accuracy) / 2;
            stopPixelWidth = calculateWorkScope(image.getWidth(), accuracy) + startPixelWidth;
            startPixelHeight = calculateWorkScope(image.getHeight(), accuracy) / 2;
            stopPixelHeight = calculateWorkScope(image.getHeight(), accuracy) + startPixelHeight;
        }
        final Raster raster = image.getRaster();
        double array[] = new double[4];
        double r = 0;
        double g = 0;
        double b = 0;

        for (int x = startPixelWidth; x < stopPixelWidth; x++) {
            for (int y = startPixelHeight; y < stopPixelHeight; y++) {
                raster.getPixel(x, y, array);
                r += array[0];
                g += array[1];
                b += array[2];
            }
        }
        final double total = (stopPixelWidth - startPixelWidth) * (stopPixelHeight - startPixelHeight);
        r /= total;
        g /= total;
        b /= total;
        return new Color((int) r, (int) g, (int) b);
    }
}
