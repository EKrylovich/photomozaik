package util;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface Calculator {
    Color averageColor(BufferedImage image, int accuracy);
}
