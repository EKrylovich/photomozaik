package controllers.sockets;

import java.io.IOException;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import log.HistoryHolder;
import log.tasks.TaskPool;


@ServerEndpoint("/get-status.do")
public class StatusSocket {

	Session session;
	
public StatusSocket() {
		
		super();
		System.out.println("Created");
		// TODO Auto-generated constructor stub
	}

	@OnOpen
	public void start(Session session) {
		this.session = session;
		System.out.println("STARTED");
	}
	
	@OnClose
	public void end() {
		this.session = null;
		System.out.println("STOPED");
	}
	
	@OnMessage
	public void message(String pId) {
		System.out.println("GET Socket MESSAGE id:" + pId);
		HistoryHolder historyHolder = TaskPool.getInstance().getTask(pId).getHistoryHolder();
		
		//TODO переделать на listner?
		
		while (true) {
						
			List<String> messagesAsJson = historyHolder.getMessagesAsJson();
			for (String message : messagesAsJson) {
				try {
					session.getBasicRemote().sendText(message);
					//session.getBasicRemote().
					System.out.println(message);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("____________nothing to log");
		}
		
		
		
	}
	
}
