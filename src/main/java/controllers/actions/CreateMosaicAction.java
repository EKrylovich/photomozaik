package controllers.actions;

import com.google.gson.Gson;
import controllers.requestHandlers.ImagesReceiver;
import controllers.requestHandlers.userSelections.UserData;
import controllers.requestHandlers.userSelections.UserDataCreator;
import model.logic.service.MainClass;
import org.apache.commons.io.FilenameUtils;
import util.DirectoryHandler;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class CreateMosaicAction implements Action {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Date d = new Date();
        String imageToProcess;
        List<String> tiles;
        

      //  String rootDirectory = DirectoryHandler.createRequestUniqueDir(request);
        //TODO это больше не утил метод. переместить его!!!
        String rootDirectory = DirectoryHandler.createRequestUniqueDir(request, request.getParameter("requestId"));

        ImagesReceiver receiver = new ImagesReceiver(request, rootDirectory);

        int photoHeight = receiver.getPhotoHeight();

        imageToProcess = receiver.receiveImgToProcess();
        //TODO человечные сообщения об ошибке в ajax. иначе вместо редиректа исключение
        if (imageToProcess == null)
            response.sendRedirect("mistake.do");

        tiles = receiver.receieveTilesList();
        if (tiles == null)
            response.sendRedirect("mistake.do");

        /*tiles = DirectoryHandler.selectOnlyImageFiles(tiles);*/

        //Это тест нового взаимодействия. Переделать в отдельный метод или удалить, в зависимости от окончательного решения.
        List<File> tilesFiles = new LinkedList<>();
        for (String tileName : tiles) {
            tilesFiles.add(new File(tileName));
        }

        Date d2 = new Date();
        System.out.println("до мэйна");
        System.out.println((d2.getTime() - d.getTime())/1000);
        //Вызов сервиса
        //TODO хз пока где это будет инициализироваться, но оно мне надо)
        UserData data = new UserDataCreator(request).getUserSelection();
        MainClass main = new MainClass(imageToProcess, tilesFiles, rootDirectory, photoHeight );
        String pathToReadyPuzzle = main.getPathToReadyPuzzle(data);


        //TODO подумать над отдельным методом
        BufferedImage sourceImage = ImageIO.read(new File(imageToProcess)), changedImage =
                ImageIO.read(new File(pathToReadyPuzzle));
        String source, changed;
        String sourceExtension = FilenameUtils.getExtension(imageToProcess);
        source = convert(sourceImage, sourceExtension);
        changed = convert(changedImage, "png");

        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("source", source);
        map.put("changed", changed);

        Date d5 = new Date();
        System.out.println("Все время");
        System.out.println((d5.getTime()-d.getTime())/1000);

        String json = new Gson().toJson(map);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

        return;

    }


    //TODO Временно (для демо). После перенести в utils
    public String convert(BufferedImage image, String type) {
        String out = null;
        //type = "jpg","png" и т.д.
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, type, Base64.getEncoder().wrap(os));
            out = os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
        }
        return out;
    }

}
