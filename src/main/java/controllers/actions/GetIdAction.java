package controllers.actions;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class GetIdAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		String id = Thread.currentThread().getName().toString() + " " + (new Date()).toString();
		id = id.replace(':', '-');
		id = id.replace(' ', '_');
		
		Gson gson = new Gson();
		
		IdWrapper wId = new IdWrapper();
		wId.id = id;
		
		String jId = gson.toJson(wId);
		
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jId);
		
	}
	
	class IdWrapper{
		String id;
	}

}
