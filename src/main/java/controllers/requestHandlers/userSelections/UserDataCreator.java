package controllers.requestHandlers.userSelections;

import javax.servlet.http.HttpServletRequest;

public class UserDataCreator {
    private HttpServletRequest request;

    public UserDataCreator(HttpServletRequest request) {
        this.request = request;
    }

    public  UserData getUserSelection(){
        UserData data = new UserData();
        boolean unique = getUniqueSelect();
        System.out.println("unique? " + unique);
        data.setUniqueFlag(unique);
        CalculatorType type = getTypeSelect();
        data.setType(type);
        System.out.println("type Calc " + type);
        if (type.equals(CalculatorType.PRECISION)){
            int accuracy = getAccuracySelect();
            data.setAccuracy(accuracy);
        }
    return data;
    }

    private int getAccuracySelect() {
        String accuracy = request.getParameter("accuracy");
        Integer value = Integer.parseInt(accuracy);
        return value;
    }

    private CalculatorType getTypeSelect() {
        CalculatorType type;
        String calculatorType = request.getParameter("analysisMode");
        if (calculatorType.equals(CalculatorType.DEFAULT.toString().toLowerCase())){
            type = CalculatorType.DEFAULT;
        }else if (calculatorType.equals(CalculatorType.HISTOGRAM.toString().toLowerCase())){
            type = CalculatorType.HISTOGRAM;
        }else {
            type = CalculatorType.PRECISION;
        }
        return type;
    }

    private boolean getUniqueSelect() {
        String uniqueParam = request.getParameter("uniqueImage");
        boolean unique = false;
        if (uniqueParam!=null){
            unique = true;
        }
        return unique;
    }

    public static void main(String[] args) {
        System.out.println(CalculatorType.DEFAULT.toString().toLowerCase());
    }
}
