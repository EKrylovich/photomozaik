package controllers.requestHandlers.userSelections;

public class UserData {
    private int accuracy = 100;
    private boolean uniqueFlag = false;
    private CalculatorType type;

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public void setUniqueFlag(boolean uniqueFlag) {
        this.uniqueFlag = uniqueFlag;
    }

    public CalculatorType getType() {
        return type;
    }

    public void setType(CalculatorType type) {
        this.type = type;
    }

    public boolean getUniqueFlag() {
        return uniqueFlag;
    }

    public int getAccuracy() {
        return accuracy;
    }


}
