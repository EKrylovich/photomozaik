package controllers.requestHandlers.userSelections;

public enum  CalculatorType {
    DEFAULT,
    PRECISION,
    HISTOGRAM
}
