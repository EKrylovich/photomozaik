package controllers.requestHandlers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import util.DirectoryHandler;

//Это плохая идея создавать такой класс, но мне кажется, что это лучше, чем писать много кода в action 
//или создавать слабосвязанную архитектуру из около 7 классов и интерфейсов вместо одного этого 
/**
 * The instance of this class is used to process request for photomosaic creation.
 *  
 * @author Chepelev Maksim
 *
 */
public class ImagesReceiver {
	
	//TODO перенести эти константы в web.xml
	/**
	 * Min number of tiles to start photomosaic creation
	 */
	private static final int MIN_TILES_COUNT = 0;
	//TODO уточнить, что значат эти переменные
	/**
	 * 
	 */
	private static final int MINHEIGHT = 50;
	private static final int MAXHEIGHT = 3000;
	
	private HttpServletRequest request;
	private String rootDirectory;
	
	/**
	 * Creates new ImagesReciver (suddenly)
	 * 
	 * @param request request to process
	 * @param rootDirectory directory for saving request data
	 */
	public ImagesReceiver(HttpServletRequest request, String rootDirectory) {
		this.request = request;
		this.rootDirectory = rootDirectory;
	}
	
	/**
	 * Receives image from user's pc or web (depends on request parameter "img_source")
	 * 
	 * @return saved image path
	 * @throws IOException
	 * @throws ServletException
	 */
	public String receiveImgToProcess() throws ServletException {
		
		String imageToProcess = null;
		
		String imgSource = request.getParameter("img_source");
		
		if (imgSource.equals("from_pc")) {
			try {
				imageToProcess = uploadFile("img_to_process");
			} catch (IOException e) {
				System.out.println(e);
			}
		} else if (imgSource.equals("by_url")) {
			imageToProcess = downloadImageByUrl(request.getParameter("img_to_process"));	
		}
		
		if (DirectoryHandler.isImageFile(imageToProcess))
			return imageToProcess;
		else
			return null;
	}

	/**
	 * Receives preferred photo height
	 * 
	 * @return photo height if success and -1 if request parameter was incorrect.
	 */
	public int getPhotoHeight()
	{
		String h = request.getParameter("photo_height");
		int height = 0;
		
		try {
			height = Integer.parseInt(h);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(height >= MINHEIGHT && height <= MAXHEIGHT)
		{
			return height;
		} else {
			return -1;
		}
		
	}
	
	/**
	 * Receives tiles from user's pc (saves only images and zip archives)
	 * 
	 * @return list of only uploaded images' files paths and images' from zip paths
	 * @throws IOException
	 * @throws ServletException
	 */
	public List<String> receieveTilesList() throws IOException, ServletException {
		
		String tilesDir = null;
		try {
			tilesDir = uploadTilesFiles("tiles");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> tiles = null;
		
		tiles = new LinkedList<>();
		
		tiles = DirectoryHandler.getFilesSpis(tilesDir);
		
		tiles = DirectoryHandler.selectOnlyImageFiles(tiles);
		//TODO временно для тестов
		System.out.println(tiles);
		
		if (tiles != null && tiles.size() > MIN_TILES_COUNT )
			return tiles;
		else
			return null;
		
	}
	
	/**
	 * 
	 * 
	 * @param url src url
	 * @return saved image path
	 */
	private String downloadImageByUrl(String url) {
		URL u;
		try {
			u = new URL(url);
			String host = u.getHost();
			
			if (host.equals("www.instagram.com")) {
				return downloadImageFromInstagram(url);
			} else {
				return downloadImageByDirectUrl(url);
			}
			
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	/**
	 * 
	 * 
	 * @author Dmitry E
	 */
	private String downloadImageByDirectUrl(String url){
		BufferedImage img;
        	String fileName = "fromlink.png";
        	try {
          		try {
           	     		img = ImageIO.read(new URL(url));
          		}
			catch (MalformedURLException m) {
                		return null;
          		}
          		File file = new File( rootDirectory + fileName);
            		if (!file.exists()) {
                		file.createNewFile();
            		}
            		ImageIO.write(img, "png", file);
        	}
        	catch(IOException e){
            		return null;
        	}
       		return rootDirectory + fileName;
	}
	
	private String downloadImageFromInstagram(String url) {
		//Я просто хотел посмотреть, как работает библиотека Jsoup,
		// поэтому здесь и появился этот код
		 try {
			Document document = Jsoup.connect(url).get();
			Elements imageMeta = document.select("meta[property=og:image]");
			String imageUrl = imageMeta.get(0).attr("content");
			String imageToProcess = downloadImageByDirectUrl(imageUrl);
			return imageToProcess;
		} catch (IOException e) {
			return null;
		}
		 
	}
	
	/*
	private String downloadImageFromGoogle(String url) {
		
		try {
			Document document = Jsoup.connect(url).get();
			Elements imageMeta = document.select("img[style=margin-top]");
			String imageUrl = imageMeta.get(0).attr("src");
			String imageToProcess = downloadImageByDirectUrl(imageUrl);
			return imageToProcess;
		} catch (IOException e) {
			return null;
		}
		
	}
	*/
	
	private String uploadFile(String parameterName) throws IOException, ServletException {
		
		Part filePart = request.getPart(parameterName);	
		return savePart(filePart, rootDirectory);
	}
	
	private String uploadTilesFiles(String parameterName) throws Exception {
		
		String tempDirectory = rootDirectory + "temp" + DirectoryHandler.FILE_SEPARATOR;
		String tilesDirectory = rootDirectory + "tiles"  + DirectoryHandler.FILE_SEPARATOR;
		File f = new File(tilesDirectory);
		f.mkdirs();
		f = new File(tempDirectory);
		f.mkdirs();
		
		Collection<Part> parts = request.getParts();
		for (Part part : parts) {
			
			if (part.getName().equals(parameterName)) {
				
				
				String partName = part.getSubmittedFileName();
				String fileName;
				if (DirectoryHandler.isImageFile(partName)){
					
					fileName = savePart(part, tilesDirectory);
				}
				else if (DirectoryHandler.isArchive(partName)) {
					fileName = savePart(part, tempDirectory);
					DirectoryHandler.unzipFile(fileName, tilesDirectory);
					
				}
				
			}
		}
		
		return tilesDirectory;		
	}
	
	private String savePart(Part filePart, String dir) {
		
		String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); 
		String fullName = dir + fileName;
		try {
			filePart.write(fullName);
		} catch (IOException e) {
			return null;
		}
		
		return fullName;
		
	}
	

}
