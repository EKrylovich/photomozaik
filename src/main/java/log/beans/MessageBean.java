package log.beans;

public class MessageBean {
	
	private String subtaskName;
	private double percents;
	private String infoText;
	//возможно лучше хранить как дату, а не строку
	private String time;
	
		
	public MessageBean(String pSubtaskName, double pPercents, String pInfoText, String pTime) {
		super();
		subtaskName = pSubtaskName;
		percents = pPercents;
		infoText = pInfoText;
		time = pTime;
	}

	protected String getSubtaskName() {
		return subtaskName;
	}

	protected void setSubtaskName(String pSubtaskName) {
		subtaskName = pSubtaskName;
	}

	protected double getPercents() {
		return percents;
	}

	protected void setPercents(double pPercents) {
		percents = pPercents;
	}

	protected String getInfoText() {
		return infoText;
	}

	protected void setInfoText(String pInfoText) {
		infoText = pInfoText;
	}

	protected String getTime() {
		return time;
	}

	protected void setTime(String pTime) {
		time = pTime;
	}

	@Override
	public String toString() {
		return "MessageBean [mSubtaskName=" + subtaskName + ", mPercents=" + percents + ", mInfoText=" + infoText
				+ ", mTime=" + time + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((infoText == null) ? 0 : infoText.hashCode());
		long temp;
		temp = Double.doubleToLongBits(percents);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((subtaskName == null) ? 0 : subtaskName.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageBean other = (MessageBean) obj;
		if (infoText == null) {
			if (other.infoText != null)
				return false;
		} else if (!infoText.equals(other.infoText))
			return false;
		if (Double.doubleToLongBits(percents) != Double.doubleToLongBits(other.percents))
			return false;
		if (subtaskName == null) {
			if (other.subtaskName != null)
				return false;
		} else if (!subtaskName.equals(other.subtaskName))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
	
	
	
	

}
