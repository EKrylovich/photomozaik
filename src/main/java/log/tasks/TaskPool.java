package log.tasks;

import java.util.HashMap;
import java.util.Map;

/**
 * Singleton. Used for create, store and manage all tasks in the app.
 * 
 * This is thread safe class
 * 
 * @author Chepelev Maksim
 *
 */
public class TaskPool {
	
	private static TaskPool pool;
	
	public static TaskPool getInstance() {
		if  (pool == null) {
			synchronized (TaskPool.class) {
				if (pool == null) {
					pool = new TaskPool();
				}
			}
		}
		
		return pool;
	}
	
	private Map<String, Task> tasks;
	
	private TaskPool() {
		tasks = new HashMap<>();
	}
	
	/**
	 * get existing task, or create it (and save it), if task doesn't exist
	 * 
	 * @param pId
	 * @return task with required id
	 */
	public synchronized Task getTask(String pId) {
		
		if (tasks.containsKey(pId)) {
			return tasks.get(pId);
		} else {
			Task task = new Task(pId);
			tasks.put(pId, task);
			return task;
		}
	}
	
	public synchronized void deleteTask(String pId) {
		tasks.remove(pId);
	}
	
	

}
