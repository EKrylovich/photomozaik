package log.tasks;

import java.util.HashMap;
import java.util.Map;

import log.HistoryHolder;

/**
 * Describes the process, that consist of several subtasks.
 * Doesn't work with process states, but contains subtask objects,
 * that work with states.
 * 
 * This is thread safe class
 * 
 * @author Chepelev Maksim
 *
 */
public class Task {
	
	private String mId;	
	private Map<String, Subtask> mSubtasks = new HashMap<>();	
	private HistoryHolder mHistoryHolder = new HistoryHolder();
	
	
	protected Task(String pId) {

		mId = pId;
	}
	
	/**
	 *  
	 * @return unique name of task
	 */
	public synchronized String getId() {
		return mId;
	}
	
	/**
	 * 
	 * @return HistoryHolder for task and it's all subtasks
	 */
	public synchronized HistoryHolder getHistoryHolder() {
		return mHistoryHolder;
	}
	
	/**
	 * 
	 * @param pName for correct work should be unique
	 * @param pNumStates number of states in this subtask. Used to percents counting
	 * @return created subtask
	 */
	public synchronized Subtask addSubtask(String pName, int pNumStates) {
		
		Subtask subtask = new Subtask(pName, pNumStates, mHistoryHolder);
		mSubtasks.put(pName, subtask);
		return subtask;
	}
	
	/**
	 * 
	 * @param pName
	 * @return subtask with required name, or null if not exists
	 */
	public synchronized Subtask getSubtask(String pName) {
		return mSubtasks.get(pName);
	}

}
