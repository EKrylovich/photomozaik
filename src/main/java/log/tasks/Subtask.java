package log.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import log.HistoryHolder;
import log.beans.MessageBean;

/**
 * The instance of this class describes the most simple process, 
 * that have some states.  
 * 
 * This is thread safe class
 *  
 * @author Chepelev Maksim
 *
 */
public class Subtask {
	
	private String mName;
	private int mNumSates;
	private int mCurrentState = 0;
	private List<String> mMessages = new ArrayList<>();
	private HistoryHolder mHistory;
	
	private boolean mDone = false;
	
	/**
	 * @throws IllegalArgumentException if pNumSates <= 0
	 * @param pNumSates
	 */
	protected Subtask(String pName, int pNumSates, HistoryHolder pHistory) {

		if (pNumSates > 0) {
			mName = pName;
			mNumSates = pNumSates;
			mHistory = pHistory;
		} else {
			throw new IllegalArgumentException();
		}		
		
	}
	
	/**
	 * adds message to a templates
	 * @param pMessage
	 */
	public synchronized void addMessageText(String pMessage) {
		mMessages.add(pMessage);
	}
	
	/**
	 * change state with random message
	 */
	public synchronized void nextState() {
		
		Random randomMessageAccess = new Random();
		int messagesNum = mMessages.size();
		if (messagesNum == 0) {
			nextState("nothing to say");
		} else {
			nextState(randomMessageAccess.nextInt(messagesNum));
		}
			
	}
	
	/**
	 * change state with message from template
	 * @param pMessageNumber
	 */
	public synchronized void nextState(int pMessageNumber) {
		
		String message;
		//TODO возможно стоит переделать
		if (mMessages.size() == 0) {
			message = "nothing to say";
		} else if (mMessages.size() <= pMessageNumber) {
			message = "too few messages";
		}else{
			message = mMessages.get(pMessageNumber);
		}
		
		nextState(message);

		
	}
	
	/**
	 * change state with custom message
	 * @throws IllegalStateException if current subtask has been finished
	 * @param pMessage
	 */
	public synchronized void nextState(String pMessage) {

		if (mDone) {
			throw new IllegalStateException("this task was closed");
		}
		
		mCurrentState++;
		
		Date currentDate = new Date();
		String info = pMessage;
		
		// может лучше *1.0?
		double percents = ((double) mCurrentState)/mNumSates;
				
		MessageBean message = new MessageBean(mName, percents, info, currentDate.toString());
				
		mHistory.addMessage(message);
				
		if (mCurrentState == mNumSates) {
					mDone = true;
		}
	}
	

}
