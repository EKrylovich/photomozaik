package log;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;


import log.beans.MessageBean;

/**
 * The entity of this class used for store all logs for a unique task	
 *
 * This is thread safe class
 * 
 * @author USER
 *
 */
public class HistoryHolder {
	
	List<MessageBean> mMessages = new ArrayList<>();
	

	public synchronized void addMessage(MessageBean pMessage) {
		mMessages.add(pMessage);
		notify();

	}
	
	/**
	 * 
	 * @return full progress log as list of json strings
	 */
	public synchronized List<String> getMessagesAsJson() {
		
		List<String> jsonMessages = new ArrayList<>();
		
		while (mMessages.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Gson gson = new Gson();
		for (MessageBean message : mMessages) {
			String jMessage = gson.toJson(message);
			jsonMessages.add(jMessage);
			
		}
		
		mMessages.clear();
		
		return jsonMessages;
		
		
		
	}

}
