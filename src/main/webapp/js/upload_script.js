
function imgSourceChanged() {

	if (document.getElementById("from_pc").checked == true) {
		document.getElementById("img_to_process_url").value = null;
		document.getElementById("img_to_process_url").disabled = true;
		document.getElementById("img_to_process_file").disabled = false;
	}

	if (document.getElementById("by_url").checked == true) {
		document.getElementById("img_to_process_file").value = null;
		document.getElementById("img_to_process_url").disabled = false;
		document.getElementById("img_to_process_file").disabled = true;
	}

}

function analysisModeChanged(){
	if($(this).val()=="precision"){
		
		$('#precisionPercent').show();
		$('#precisionLabel').show();
	}
	else
		{
		$('#precisionPercent').hide();
		$('#precisionLabel').hide();
		}
}

function precisionPercentChanged(){
	var percentValue = $('#precisionPercent').val();
	document.getElementById('precisionLabel').value = percentValue + "%";
}

function successAnswer(JsonObject) {

	//json to base64 encoding
	var base64Source = JsonObject.source;
	var base64Changed = JsonObject.changed;

	var encoder = 'data:image/png;base64,'
	var source = encoder.concat(base64Source);
	var changed = encoder.concat(base64Changed);

	var sourceImage = $('<img alt="Source" id="sourceImage" src="">');
	$('body').append(sourceImage);
	$('#sourceImage').attr('src',source);

	var changedImage = $('<img alt="Changed" id="changedImage" src="">');
	$('body').append(changedImage);
	$('#changedImage').attr('src',changed);

	var downloadButton= $('<input type="button" value="Download" id="download"/>');
	$('body').append(downloadButton);

	$('#download').click(function(event){
		document.getElementById('downloadLink').click();
	});
	$('#downloadLink').attr('href',changed);
	$('#downloadLink').attr('download','changed.jpg');

};


$(document).ready(function() {
		$('#analysisMode').change(analysisModeChanged);
		$('#precisionPercent').change(precisionPercentChanged);
	
		$('#submitForm').submit(function (event) {
			 event.preventDefault();
			 var formData = new FormData($('#submitForm')[0]);
			 $('#submitForm').hide();
			 $.ajax({
		            url: 'create_mosaic.do',
		            type: 'POST',
		            data: formData,
		            async: false,
		            cache: false,
		            contentType: false,
		            processData: false,
		            success: successAnswer,
		            error: function(){
		                alert("error in ajax form submission");
		            }
		        });

	});
});
